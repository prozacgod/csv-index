So this may not really be solving any unique problem domains, and is honestly a solution kinda looking for a problem

I was reading a conversation with some people over on /r/DataHoarder and chimed in my 2 cents

https://www.reddit.com/r/DataHoarder/comments/9cy6gw/this_data_hoarder_is_downloading_the_metadata_of/e5ep4k3/


The idea is to read in a CSV file, and then built up a list of offsets to each column and parse the length for every row inside an entire file.

The index in this example is almost the same size as the CSV

Some constraints, to me this needed to be a very straight forware encoding mechanism, something that would be stream in -> stream out.

While the ... really really dirty example here does not do that, I suspect it could very easily.  This means it's processing friendly, and also should be fast to work with

And that the output is very predictable knowing as little information about the original csv file as possible.

This encodes each row of a CSV file as follows

2 byte unsigned short for the number of columns
4 byte offset to the start of this record
4 byte length of this record

then column * number of the following

4 byte offset to the start of a record
2 byte length for the record

All records are assumed to be strings


csv_idx.py is ... hastily written hard coded to '\r' carriage returns (the data I have in the directory is this way)

LineIndexer.py is a statful parsing loop with actually nothing complicated, and it returns the meta data for a whole line in a text file.

reader.py is an example on how to use the index data to read the CSV file data in a "record based" approach. TO my surprise it seemed to work out of the box!!




