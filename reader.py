import struct

HEADERFORMAT = "HLL"
COLUMNFORMAT = "LH"

def decodeLine(data):
    result = struct.unpack(HEADERFORMAT, data)
    for column in row.columns:
        result += struct.pack(COLUMNFORMAT, column.start, column.length)

    return result

# due to the choice in formating, the column count is encoded into the first 2 bytes
# giving me all the information I need to see rows

class IdxReader:
    def __init__(self, fname):
        self.csv_name = fname + '.csv';
        self.csv_file = open(self.csv_name, 'rb')

        self.idx_name = fname + '.csdx';
        self.idx_file = open(self.idx_name, 'rb')

        self.column_count = struct.unpack('<H', self.idx_file.read(2))[0]
        print "CC:", self.column_count
        self.row_fmt = '<' + HEADERFORMAT + (self.column_count * COLUMNFORMAT)
        self.row_size = struct.calcsize(self.row_fmt)

    def getRowMeta(self, index):
        self.idx_file.seek(index * self.row_size)
        data = self.idx_file.read(self.row_size)

        return struct.unpack(self.row_fmt, data)

    def getRowData(self, index):
        meta = self.getRowMeta(index)
        columns = meta[3:]
        result = []
        print len(columns)
        for index in range(meta[0]):
            self.csv_file.seek(columns[index * 2 + 0])
            result += [self.csv_file.read(columns[index * 2 + 1])]

        return result

db = IdxReader("FL_insurance_sample")
a = db.getRowData(0)
print a
d = db.getRowData(1234)
print d
