import struct

# my philosophy on this data indexer is that only EOL and commas aren't indexed.
# x,y,z,"Dinosaur Space"
# would encode 1 byte limits for x y z and
# 16 bytes for "Dinosaur Space"
# white space immediately after the comma could also be considered 'part of the column'
# as a white space trimmer, and a quoted decoder aren't important to the indexing
# with this philosophy, the header is just encoded like anything else.

# Data snagged from here https://support.spatialkey.com/spatialkey-sample-csv-data/

LINE_ENDING = '\r'

class Column:
    def __init__(self, start, length):
        self.start = start
        self.length = length

# line endings, escapeing etc... obviously unimplemented
class LineIndexer:
    def __init__(self, data, start_offset=0, length=0):
        self.columns = []
        self.start = start_offset
        self.length = 0

        current_index_start = None
        row_index = start_offset
        while row_index < length:
            if current_index_start == None:
                current_index_start = row_index

            if (data[row_index] == ord(LINE_ENDING)):
                if current_index_start != None:
                    self.columns += [Column(current_index_start, row_index - current_index_start)]

                self.length = row_index - start_offset + 1
                return

            if data[row_index] == ord(','):
                self.columns += [Column(current_index_start, row_index - current_index_start)]
                current_index_start = None;

            row_index += 1

        self.length = row_index - start_offset + 1
        if current_index_start != None:
            self.columns += [Column(current_index_start, row_index - current_index_start)]

def hexDump(data):
    print ":".join("{:02x}".format(ord(c)) for c in data)

HEADERFORMAT = "<HLL"
COLUMNFORMAT = "<LH"

def encodeLineMeta(row):
    result = struct.pack(HEADERFORMAT, len(row.columns), row.start, row.length)
    for column in row.columns:
        result += struct.pack(COLUMNFORMAT, column.start, column.length)

    return result

    # line format

    # uint16 column count
    # uint32 start
    # uint32 length
    # [ uint32 data offset, uint16 data length ]

    # - I should probably enforce all lines have the same 'count' columns during encoding

    # - supporting more than 65k columns seems ... a bit odd?
    # - I'm assuming 65k for a whole column is .. enough?  BUT I can see edge cases beyond that
    # - offsets could easily be 64bit
    # - a line needs a count of columns, and the start/length of each column
    #
    # [ uint32 data offset, uint16 data length ]  65k data for a whole column???

fname = "FL_insurance_sample.csv"

# ... what is unicode anyway... :/  (obviously not in this dirty example)
# also just reading in the whole damned file, because...
# this format should handle stream based encoding well enough.

data = bytearray(open(fname, 'rb').read())

line = LineIndexer(
    data = data,
    start_offset = 0,
    length = len(data)
)
rawRow = encodeLineMeta(line)

print "row length", len(rawRow), len(line.columns)

column_count = None
offset = 0

output = open('FL_insurance_sample.csdx', 'wb')
row_count = 0
while True:
    line = LineIndexer(
        data = data,
        start_offset = offset,
        length = len(data)
    )

    if column_count == None:
        column_count = len(line.columns)
    elif column_count != len(line.columns):
        raise "Unmatched column count expected: %d actual: %d" % ( column_count, len(line.column) )

    output.write(encodeLineMeta(line))

    offset += line.length
    row_count += 1

    if offset >= len(data):
        break

output.close()

print "count: ", row_count


#size = (2 + 4 + 4 + (18 * (4 + 2)))
#print "Record Size:", size
#print "Record Count:", 5715060/size
